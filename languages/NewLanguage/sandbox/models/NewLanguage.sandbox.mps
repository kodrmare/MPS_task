<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:75c48f56-9553-4940-a885-9905446abbaf(NewLanguage.sandbox)">
  <persistence version="9" />
  <languages>
    <use id="8382b9f2-6c63-4138-b944-443f95dadf25" name="NewLanguage" version="-1" />
    <use id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage" version="5" />
    <use id="f2801650-65d5-424e-bb1b-463a8781b786" name="jetbrains.mps.baseLanguage.javadoc" version="2" />
  </languages>
  <imports>
    <import index="wyt6" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.lang(JDK/)" />
    <import index="hyam" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.awt.event(JDK/)" />
    <import index="z60i" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.awt(JDK/)" />
    <import index="dxuu" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:javax.swing(JDK/)" />
    <import index="guwi" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.io(JDK/)" />
    <import index="m4oy" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:javax.swing.plaf.metal(JDK/)" />
    <import index="zf81" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.net(JDK/)" />
    <import index="9z78" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:javax.swing.border(JDK/)" />
  </imports>
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1202948039474" name="jetbrains.mps.baseLanguage.structure.InstanceMethodCallOperation" flags="nn" index="liA8E" />
      <concept id="1465982738277781862" name="jetbrains.mps.baseLanguage.structure.PlaceholderMember" flags="ng" index="2tJIrI" />
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1081236700938" name="jetbrains.mps.baseLanguage.structure.StaticMethodDeclaration" flags="ig" index="2YIFZL" />
      <concept id="1070533707846" name="jetbrains.mps.baseLanguage.structure.StaticFieldReference" flags="nn" index="10M0yZ">
        <reference id="1144433057691" name="classifier" index="1PxDUh" />
      </concept>
      <concept id="1070534370425" name="jetbrains.mps.baseLanguage.structure.IntegerType" flags="in" index="10Oyi0" />
      <concept id="1070534760951" name="jetbrains.mps.baseLanguage.structure.ArrayType" flags="in" index="10Q1$e">
        <child id="1070534760952" name="componentType" index="10Q1$1" />
      </concept>
      <concept id="1068390468198" name="jetbrains.mps.baseLanguage.structure.ClassConcept" flags="ig" index="312cEu" />
      <concept id="1068431474542" name="jetbrains.mps.baseLanguage.structure.VariableDeclaration" flags="ng" index="33uBYm">
        <child id="1068431790190" name="initializer" index="33vP2m" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1068498886292" name="jetbrains.mps.baseLanguage.structure.ParameterDeclaration" flags="ir" index="37vLTG" />
      <concept id="1225271177708" name="jetbrains.mps.baseLanguage.structure.StringType" flags="in" index="17QB3L" />
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123132" name="jetbrains.mps.baseLanguage.structure.BaseMethodDeclaration" flags="ng" index="3clF44">
        <property id="4276006055363816570" name="isSynchronized" index="od$2w" />
        <property id="1181808852946" name="isFinal" index="DiZV1" />
        <child id="1068580123133" name="returnType" index="3clF45" />
        <child id="1068580123134" name="parameter" index="3clF46" />
        <child id="1068580123135" name="body" index="3clF47" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123157" name="jetbrains.mps.baseLanguage.structure.Statement" flags="nn" index="3clFbH" />
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068580320020" name="jetbrains.mps.baseLanguage.structure.IntegerConstant" flags="nn" index="3cmrfG">
        <property id="1068580320021" name="value" index="3cmrfH" />
      </concept>
      <concept id="1068581242875" name="jetbrains.mps.baseLanguage.structure.PlusExpression" flags="nn" index="3cpWs3" />
      <concept id="1068581242864" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclarationStatement" flags="nn" index="3cpWs8">
        <child id="1068581242865" name="localVariableDeclaration" index="3cpWs9" />
      </concept>
      <concept id="1068581242869" name="jetbrains.mps.baseLanguage.structure.MinusExpression" flags="nn" index="3cpWsd" />
      <concept id="1068581242863" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclaration" flags="nr" index="3cpWsn" />
      <concept id="1068581517677" name="jetbrains.mps.baseLanguage.structure.VoidType" flags="in" index="3cqZAl" />
      <concept id="1079359253375" name="jetbrains.mps.baseLanguage.structure.ParenthesizedExpression" flags="nn" index="1eOMI4">
        <child id="1079359253376" name="expression" index="1eOMHV" />
      </concept>
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
        <child id="1068499141038" name="actualArgument" index="37wK5m" />
      </concept>
      <concept id="1107461130800" name="jetbrains.mps.baseLanguage.structure.Classifier" flags="ng" index="3pOWGL">
        <child id="5375687026011219971" name="member" index="jymVt" unordered="true" />
      </concept>
      <concept id="7812454656619025416" name="jetbrains.mps.baseLanguage.structure.MethodDeclaration" flags="ng" index="1rXfSm">
        <property id="8355037393041754995" name="isNative" index="2aFKle" />
      </concept>
      <concept id="1081773326031" name="jetbrains.mps.baseLanguage.structure.BinaryOperation" flags="nn" index="3uHJSO">
        <child id="1081773367579" name="rightExpression" index="3uHU7w" />
        <child id="1081773367580" name="leftExpression" index="3uHU7B" />
      </concept>
      <concept id="1178549954367" name="jetbrains.mps.baseLanguage.structure.IVisible" flags="ng" index="1B3ioH">
        <child id="1178549979242" name="visibility" index="1B3o_S" />
      </concept>
      <concept id="1146644602865" name="jetbrains.mps.baseLanguage.structure.PublicVisibility" flags="nn" index="3Tm1VV" />
    </language>
    <language id="8382b9f2-6c63-4138-b944-443f95dadf25" name="NewLanguage">
      <concept id="1003592845743397087" name="NewLanguage.structure.Bool_Variable" flags="ng" index="KhuaZ">
        <property id="1003592845743397088" name="value" index="Khua0" />
      </concept>
      <concept id="1003592845743172747" name="NewLanguage.structure.Int_Variable" flags="ng" index="Ki5rF">
        <property id="1003592845743172752" name="value" index="Ki5rK" />
      </concept>
      <concept id="1003592845743198237" name="NewLanguage.structure.IntReference" flags="ng" index="KieDX">
        <reference id="1003592845743198238" name="field" index="KieDY" />
      </concept>
      <concept id="1003592845743265126" name="NewLanguage.structure.String_Variable" flags="ng" index="KiYW6">
        <property id="1003592845743265127" name="value" index="KiYW7" />
      </concept>
      <concept id="1003592845742852261" name="NewLanguage.structure.InputValue" flags="ng" index="Kjjb5">
        <property id="1003592845742852264" name="value" index="Kjjb8" />
      </concept>
      <concept id="1003592845742886910" name="NewLanguage.structure.ExpressionVariable" flags="ng" index="Kjrmu">
        <child id="1003592845742918536" name="expression" index="Kj37C" />
      </concept>
      <concept id="1003592845743092665" name="NewLanguage.structure.SomeCode" flags="ng" index="KjCBp" />
      <concept id="1003592845742783447" name="NewLanguage.structure.To" flags="ng" index="Ks$6R" />
      <concept id="1003592845742774188" name="NewLanguage.structure.From" flags="ng" index="KsARc" />
      <concept id="1003592845742817427" name="NewLanguage.structure.Body" flags="ng" index="KsFFN">
        <child id="1003592845743092660" name="iterate" index="KjCBk" />
        <child id="1003592845743092662" name="code" index="KjCBm" />
      </concept>
      <concept id="1003592845742766569" name="NewLanguage.structure.Variable" flags="ng" index="KsSe9" />
      <concept id="1003592845742758661" name="NewLanguage.structure.Script" flags="ng" index="KsU5_">
        <child id="1003592845743189324" name="variable" index="Ki1sG" />
        <child id="1003592845742852340" name="int" index="Kjjak" />
        <child id="1003592845742758665" name="loops" index="KsU5D" />
      </concept>
      <concept id="1003592845742757299" name="NewLanguage.structure.Iterate" flags="ng" index="KsUZj">
        <child id="6700851436499294851" name="statement" index="2xMITz" />
        <child id="1003592845742783456" name="to" index="Ks$60" />
        <child id="1003592845742783461" name="val_to" index="Ks$65" />
        <child id="1003592845742783452" name="val_from" index="Ks$6W" />
        <child id="1003592845742774199" name="from" index="KsARn" />
        <child id="1003592845742821611" name="body" index="KsEEb" />
        <child id="1003592845742766574" name="val" index="KsSee" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="KsU5_" id="RHuA0XBI3h">
    <property role="TrG5h" value="MyCode" />
    <node concept="KsUZj" id="RHuA0XDy1j" role="KsU5D">
      <node concept="KsFFN" id="RHuA0XD$iO" role="KsEEb">
        <node concept="KsUZj" id="RHuA0XDIoY" role="KjCBk">
          <node concept="KsSe9" id="RHuA0XDIoZ" role="KsSee">
            <property role="TrG5h" value="b" />
          </node>
          <node concept="KsARc" id="RHuA0XDIp0" role="KsARn" />
          <node concept="Kjrmu" id="RHuA0XDIp1" role="Ks$6W">
            <node concept="3cmrfG" id="RHuA0XDIpu" role="Kj37C">
              <property role="3cmrfH" value="1" />
            </node>
          </node>
          <node concept="Ks$6R" id="RHuA0XDIp3" role="Ks$60" />
          <node concept="Kjrmu" id="RHuA0XDIp4" role="Ks$65">
            <node concept="3cmrfG" id="RHuA0XDIp_" role="Kj37C">
              <property role="3cmrfH" value="4" />
            </node>
          </node>
          <node concept="KsFFN" id="RHuA0XDIr0" role="KsEEb">
            <node concept="KjCBp" id="RHuA0XDIr4" role="KjCBm" />
          </node>
        </node>
      </node>
      <node concept="KsSe9" id="RHuA0XDy1k" role="KsSee">
        <property role="TrG5h" value="i" />
      </node>
      <node concept="KsARc" id="RHuA0XDy1l" role="KsARn" />
      <node concept="Kjrmu" id="RHuA0XDy1m" role="Ks$6W">
        <node concept="3cmrfG" id="RHuA0XDAGu" role="Kj37C">
          <property role="3cmrfH" value="12" />
        </node>
      </node>
      <node concept="Ks$6R" id="RHuA0XDy1o" role="Ks$60" />
      <node concept="Kjrmu" id="RHuA0XDy1p" role="Ks$65">
        <node concept="3cpWsd" id="RHuA0XEe_T" role="Kj37C">
          <node concept="KieDX" id="RHuA0XEe_W" role="3uHU7w">
            <ref role="KieDY" node="RHuA0XEe_w" resolve="b" />
          </node>
          <node concept="1eOMI4" id="RHuA0XDPDP" role="3uHU7B">
            <node concept="3cpWs3" id="RHuA0XDAXu" role="1eOMHV">
              <node concept="3cmrfG" id="RHuA0XDAG_" role="3uHU7B">
                <property role="3cmrfH" value="34" />
              </node>
              <node concept="KieDX" id="RHuA0XE9yo" role="3uHU7w">
                <ref role="KieDY" node="RHuA0XDAGq" resolve="a" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="Kjjb5" id="RHuA0XC85T" role="Kjjak">
      <property role="TrG5h" value="a" />
      <property role="Kjjb8" value="4" />
    </node>
    <node concept="Ki5rF" id="RHuA0XDAGq" role="Ki1sG">
      <property role="TrG5h" value="a" />
      <property role="Ki5rK" value="23" />
    </node>
    <node concept="KiYW6" id="RHuA0XDFS7" role="Ki1sG">
      <property role="TrG5h" value="name" />
      <property role="KiYW7" value="someString" />
    </node>
    <node concept="KhuaZ" id="RHuA0XEe$B" role="Ki1sG">
      <property role="TrG5h" value="flag" />
      <property role="Khua0" value="true" />
    </node>
    <node concept="Ki5rF" id="RHuA0XEe_w" role="Ki1sG">
      <property role="TrG5h" value="b" />
      <property role="Ki5rK" value="4" />
    </node>
    <node concept="Ki5rF" id="RHuA0XE9zk" role="Ki1sG">
      <property role="TrG5h" value="counter" />
      <property role="Ki5rK" value="0" />
    </node>
  </node>
  <node concept="312cEu" id="5NYde3XcILY">
    <property role="TrG5h" value="testClass" />
    <node concept="2tJIrI" id="5NYde3XcIMT" role="jymVt" />
    <node concept="2YIFZL" id="5NYde3XdaZf" role="jymVt">
      <property role="TrG5h" value="main" />
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <node concept="3clFbS" id="5NYde3XdaZi" role="3clF47">
        <node concept="3cpWs8" id="5NYde3Xdbcv" role="3cqZAp">
          <node concept="3cpWsn" id="5NYde3Xdbcy" role="3cpWs9">
            <property role="TrG5h" value="a" />
            <node concept="10Oyi0" id="5NYde3Xdbcu" role="1tU5fm" />
            <node concept="3cmrfG" id="5NYde3Xdbha" role="33vP2m">
              <property role="3cmrfH" value="10" />
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="5NYde3Xddkb" role="3cqZAp" />
        <node concept="KsUZj" id="5NYde3Xdbpf" role="3cqZAp">
          <node concept="KsSe9" id="5NYde3Xdbph" role="KsSee">
            <property role="TrG5h" value="i" />
          </node>
          <node concept="KsARc" id="5NYde3Xdbpj" role="KsARn" />
          <node concept="Kjrmu" id="5NYde3Xdbpl" role="Ks$6W">
            <node concept="3cmrfG" id="5NYde3XdbxH" role="Kj37C">
              <property role="3cmrfH" value="3" />
            </node>
          </node>
          <node concept="Ks$6R" id="5NYde3Xdbpp" role="Ks$60" />
          <node concept="Kjrmu" id="5NYde3Xdbpr" role="Ks$65">
            <node concept="3cpWs3" id="5NYde3XdceW" role="Kj37C">
              <node concept="3cmrfG" id="5NYde3XdbH2" role="3uHU7B">
                <property role="3cmrfH" value="4" />
              </node>
              <node concept="3cmrfG" id="5NYde3XdciG" role="3uHU7w">
                <property role="3cmrfH" value="4" />
              </node>
            </node>
          </node>
          <node concept="KsFFN" id="5NYde3XddDP" role="KsEEb" />
          <node concept="3clFbF" id="5NYde3Xdirf" role="2xMITz">
            <node concept="2OqwBi" id="5NYde3XdixH" role="3clFbG">
              <node concept="10M0yZ" id="5NYde3XdirS" role="2Oq$k0">
                <ref role="3cqZAo" to="wyt6:~System.out" resolve="out" />
                <ref role="1PxDUh" to="wyt6:~System" resolve="System" />
              </node>
              <node concept="liA8E" id="5NYde3XdiJ2" role="2OqNvi">
                <ref role="37wK5l" to="guwi:~PrintStream.println(java.lang.String):void" resolve="println" />
                <node concept="Xl_RD" id="5NYde3XdiNw" role="37wK5m">
                  <property role="Xl_RC" value="Iterating" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3clFbF" id="5NYde3Xdppr" role="2xMITz">
            <node concept="2OqwBi" id="5NYde3Xdp_X" role="3clFbG">
              <node concept="10M0yZ" id="5NYde3Xdpr4" role="2Oq$k0">
                <ref role="3cqZAo" to="wyt6:~System.out" resolve="out" />
                <ref role="1PxDUh" to="wyt6:~System" resolve="System" />
              </node>
              <node concept="liA8E" id="5NYde3XdpGO" role="2OqNvi">
                <ref role="37wK5l" to="guwi:~PrintStream.println(java.lang.String):void" resolve="println" />
                <node concept="Xl_RD" id="5NYde3XdpL5" role="37wK5m">
                  <property role="Xl_RC" value="Surrent value: " />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="5NYde3Xdqwa" role="3cqZAp">
          <node concept="2OqwBi" id="5NYde3XdqCG" role="3clFbG">
            <node concept="10M0yZ" id="5NYde3Xdqyt" role="2Oq$k0">
              <ref role="3cqZAo" to="wyt6:~System.out" resolve="out" />
              <ref role="1PxDUh" to="wyt6:~System" resolve="System" />
            </node>
            <node concept="liA8E" id="5NYde3XdqKd" role="2OqNvi">
              <ref role="37wK5l" to="guwi:~PrintStream.println(java.lang.String):void" resolve="println" />
              <node concept="Xl_RD" id="5NYde3XdqOu" role="37wK5m">
                <property role="Xl_RC" value="Done" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm1VV" id="5NYde3XdaVh" role="1B3o_S" />
      <node concept="3cqZAl" id="5NYde3XdaZ7" role="3clF45" />
      <node concept="37vLTG" id="5NYde3Xdb1u" role="3clF46">
        <property role="TrG5h" value="args" />
        <node concept="10Q1$e" id="5NYde3Xdb6O" role="1tU5fm">
          <node concept="17QB3L" id="5NYde3Xdb1t" role="10Q1$1" />
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="5NYde3XcIMW" role="jymVt" />
    <node concept="3Tm1VV" id="5NYde3XcILZ" role="1B3o_S" />
  </node>
</model>

