<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:93c6569a-4020-448e-914e-5eb8bc7a3d46(NewLanguage.structure)">
  <persistence version="9" />
  <languages>
    <use id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure" version="3" />
    <devkit ref="2677cb18-f558-4e33-bc38-a5139cee06dc(jetbrains.mps.devkit.language-design)" />
  </languages>
  <imports>
    <import index="tpee" ref="r:00000000-0000-4000-0000-011c895902ca(jetbrains.mps.baseLanguage.structure)" implicit="true" />
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure">
      <concept id="1169125787135" name="jetbrains.mps.lang.structure.structure.AbstractConceptDeclaration" flags="ig" index="PkWjJ">
        <property id="6714410169261853888" name="conceptId" index="EcuMT" />
        <property id="4628067390765956807" name="final" index="R5$K2" />
        <property id="4628067390765956802" name="abstract" index="R5$K7" />
        <property id="5092175715804935370" name="conceptAlias" index="34LRSv" />
        <child id="1071489727083" name="linkDeclaration" index="1TKVEi" />
        <child id="1071489727084" name="propertyDeclaration" index="1TKVEl" />
      </concept>
      <concept id="1169127622168" name="jetbrains.mps.lang.structure.structure.InterfaceConceptReference" flags="ig" index="PrWs8">
        <reference id="1169127628841" name="intfc" index="PrY4T" />
      </concept>
      <concept id="1071489090640" name="jetbrains.mps.lang.structure.structure.ConceptDeclaration" flags="ig" index="1TIwiD">
        <property id="1096454100552" name="rootable" index="19KtqR" />
        <reference id="1071489389519" name="extends" index="1TJDcQ" />
        <child id="1169129564478" name="implements" index="PzmwI" />
      </concept>
      <concept id="1071489288299" name="jetbrains.mps.lang.structure.structure.PropertyDeclaration" flags="ig" index="1TJgyi">
        <property id="241647608299431129" name="propertyId" index="IQ2nx" />
        <reference id="1082985295845" name="dataType" index="AX2Wp" />
      </concept>
      <concept id="1071489288298" name="jetbrains.mps.lang.structure.structure.LinkDeclaration" flags="ig" index="1TJgyj">
        <property id="1071599776563" name="role" index="20kJfa" />
        <property id="1071599893252" name="sourceCardinality" index="20lbJX" />
        <property id="1071599937831" name="metaClass" index="20lmBu" />
        <property id="241647608299431140" name="linkId" index="IQ2ns" />
        <reference id="1071599976176" name="target" index="20lvS9" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="1TIwiD" id="RHuA0XBHmN">
    <property role="EcuMT" value="1003592845742757299" />
    <property role="TrG5h" value="Iterate" />
    <property role="34LRSv" value="iterate" />
    <ref role="1TJDcQ" to="tpee:fzclF8l" resolve="Statement" />
    <node concept="1TJgyj" id="RHuA0XBJBI" role="1TKVEi">
      <property role="IQ2ns" value="1003592845742766574" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="val" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="RHuA0XBJBD" resolve="Variable" />
    </node>
    <node concept="1TJgyj" id="RHuA0XBLuR" role="1TKVEi">
      <property role="IQ2ns" value="1003592845742774199" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="from" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="RHuA0XBLuG" resolve="From" />
    </node>
    <node concept="1TJgyj" id="RHuA0XBNJs" role="1TKVEi">
      <property role="IQ2ns" value="1003592845742783452" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="val_from" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="RHuA0XCcZY" resolve="ExpressionVariable" />
    </node>
    <node concept="1TJgyj" id="RHuA0XBNJw" role="1TKVEi">
      <property role="IQ2ns" value="1003592845742783456" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="to" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="RHuA0XBNJn" resolve="To" />
    </node>
    <node concept="1TJgyj" id="RHuA0XBNJ_" role="1TKVEi">
      <property role="IQ2ns" value="1003592845742783461" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="val_to" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="RHuA0XCcZY" resolve="ExpressionVariable" />
    </node>
    <node concept="1TJgyj" id="RHuA0XBX3F" role="1TKVEi">
      <property role="IQ2ns" value="1003592845742821611" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="body" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" node="RHuA0XBW2j" resolve="Body" />
    </node>
    <node concept="1TJgyj" id="5NYde3XddE3" role="1TKVEi">
      <property role="IQ2ns" value="6700851436499294851" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="statement" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" to="tpee:fzclF8l" resolve="Statement" />
    </node>
  </node>
  <node concept="1TIwiD" id="RHuA0XBHG5">
    <property role="EcuMT" value="1003592845742758661" />
    <property role="TrG5h" value="Script" />
    <property role="19KtqR" value="true" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyj" id="RHuA0XDmPc" role="1TKVEi">
      <property role="IQ2ns" value="1003592845743189324" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="variable" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" node="RHuA0XDgP4" resolve="AbstractVariable" />
    </node>
    <node concept="1TJgyj" id="RHuA0XC4zO" role="1TKVEi">
      <property role="IQ2ns" value="1003592845742852340" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="int" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="RHuA0XC4y_" resolve="InputValue" />
    </node>
    <node concept="PrWs8" id="RHuA0XBHG6" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
    <node concept="1TJgyj" id="RHuA0XBHG9" role="1TKVEi">
      <property role="IQ2ns" value="1003592845742758665" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="loops" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" node="RHuA0XBHmN" resolve="Iterate" />
    </node>
  </node>
  <node concept="1TIwiD" id="RHuA0XBJBD">
    <property role="EcuMT" value="1003592845742766569" />
    <property role="TrG5h" value="Variable" />
    <property role="34LRSv" value="variable" />
    <property role="3GE5qa" value="variables" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="PrWs8" id="RHuA0XBJBE" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
    <node concept="1TJgyi" id="RHuA0XBJBG" role="1TKVEl">
      <property role="IQ2nx" value="1003592845742766572" />
      <property role="TrG5h" value="value" />
      <ref role="AX2Wp" to="tpck:fKAQMTA" resolve="integer" />
    </node>
  </node>
  <node concept="1TIwiD" id="RHuA0XBLuG">
    <property role="EcuMT" value="1003592845742774188" />
    <property role="TrG5h" value="From" />
    <property role="34LRSv" value="from" />
    <property role="3GE5qa" value="range iterators name" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
  </node>
  <node concept="1TIwiD" id="RHuA0XBNJn">
    <property role="EcuMT" value="1003592845742783447" />
    <property role="TrG5h" value="To" />
    <property role="34LRSv" value="to" />
    <property role="3GE5qa" value="range iterators name" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
  </node>
  <node concept="1TIwiD" id="RHuA0XBW2j">
    <property role="EcuMT" value="1003592845742817427" />
    <property role="TrG5h" value="Body" />
    <property role="34LRSv" value="body" />
    <property role="3GE5qa" value="others" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="PrWs8" id="RHuA0XBW2m" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
    <node concept="1TJgyj" id="RHuA0XCZeO" role="1TKVEi">
      <property role="IQ2ns" value="1003592845743092660" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="iterate" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" node="RHuA0XBHmN" resolve="Iterate" />
    </node>
    <node concept="1TJgyj" id="RHuA0XCZeQ" role="1TKVEi">
      <property role="IQ2ns" value="1003592845743092662" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="code" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" node="RHuA0XCZeT" resolve="SomeCode" />
    </node>
  </node>
  <node concept="1TIwiD" id="RHuA0XC4y_">
    <property role="EcuMT" value="1003592845742852261" />
    <property role="TrG5h" value="InputValue" />
    <property role="3GE5qa" value="variables" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="PrWs8" id="RHuA0XC4yA" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
    <node concept="1TJgyi" id="RHuA0XC4yC" role="1TKVEl">
      <property role="IQ2nx" value="1003592845742852264" />
      <property role="TrG5h" value="value" />
      <ref role="AX2Wp" to="tpck:fKAQMTA" resolve="integer" />
    </node>
  </node>
  <node concept="1TIwiD" id="RHuA0XCcZY">
    <property role="EcuMT" value="1003592845742886910" />
    <property role="TrG5h" value="ExpressionVariable" />
    <property role="3GE5qa" value="variables" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyj" id="RHuA0XCkI8" role="1TKVEi">
      <property role="IQ2ns" value="1003592845742918536" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="expression" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" to="tpee:fz3vP1J" resolve="Expression" />
    </node>
  </node>
  <node concept="1TIwiD" id="RHuA0XC$to">
    <property role="EcuMT" value="1003592845742983000" />
    <property role="TrG5h" value="VariableReference" />
    <property role="3GE5qa" value="variables" />
    <ref role="1TJDcQ" to="tpee:fz3vP1J" resolve="Expression" />
    <node concept="1TJgyj" id="RHuA0XC$tp" role="1TKVEi">
      <property role="IQ2ns" value="1003592845742983001" />
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="field" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="RHuA0XC4y_" resolve="InputValue" />
    </node>
  </node>
  <node concept="1TIwiD" id="RHuA0XCZeT">
    <property role="EcuMT" value="1003592845743092665" />
    <property role="TrG5h" value="SomeCode" />
    <property role="3GE5qa" value="others" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
  </node>
  <node concept="1TIwiD" id="RHuA0XDgP4">
    <property role="EcuMT" value="1003592845743164740" />
    <property role="3GE5qa" value="variables" />
    <property role="TrG5h" value="AbstractVariable" />
    <property role="R5$K7" value="true" />
    <property role="R5$K2" value="false" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
  </node>
  <node concept="1TIwiD" id="RHuA0XDiMb">
    <property role="EcuMT" value="1003592845743172747" />
    <property role="3GE5qa" value="variables" />
    <property role="TrG5h" value="Int_Variable" />
    <property role="34LRSv" value="int" />
    <ref role="1TJDcQ" node="RHuA0XDgP4" resolve="AbstractVariable" />
    <node concept="PrWs8" id="RHuA0XDiMc" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
    <node concept="1TJgyi" id="RHuA0XDiMg" role="1TKVEl">
      <property role="IQ2nx" value="1003592845743172752" />
      <property role="TrG5h" value="value" />
      <ref role="AX2Wp" to="tpck:fKAQMTA" resolve="integer" />
    </node>
  </node>
  <node concept="1TIwiD" id="RHuA0XDp0t">
    <property role="EcuMT" value="1003592845743198237" />
    <property role="3GE5qa" value="variables" />
    <property role="TrG5h" value="IntReference" />
    <ref role="1TJDcQ" to="tpee:fz3vP1J" resolve="Expression" />
    <node concept="1TJgyj" id="RHuA0XDp0u" role="1TKVEi">
      <property role="IQ2ns" value="1003592845743198238" />
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="field" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="RHuA0XDiMb" resolve="Int_Variable" />
    </node>
  </node>
  <node concept="1TIwiD" id="RHuA0XDDlA">
    <property role="EcuMT" value="1003592845743265126" />
    <property role="3GE5qa" value="variables" />
    <property role="TrG5h" value="String_Variable" />
    <property role="34LRSv" value="string" />
    <ref role="1TJDcQ" node="RHuA0XDgP4" resolve="AbstractVariable" />
    <node concept="1TJgyi" id="RHuA0XDDlB" role="1TKVEl">
      <property role="IQ2nx" value="1003592845743265127" />
      <property role="TrG5h" value="value" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="PrWs8" id="RHuA0XDDlF" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="RHuA0XE9zv">
    <property role="EcuMT" value="1003592845743397087" />
    <property role="3GE5qa" value="variables" />
    <property role="TrG5h" value="Bool_Variable" />
    <property role="34LRSv" value="bool" />
    <ref role="1TJDcQ" node="RHuA0XDgP4" resolve="AbstractVariable" />
    <node concept="1TJgyi" id="RHuA0XE9zw" role="1TKVEl">
      <property role="IQ2nx" value="1003592845743397088" />
      <property role="TrG5h" value="value" />
      <ref role="AX2Wp" to="tpck:fKAQMTB" resolve="boolean" />
    </node>
    <node concept="PrWs8" id="RHuA0XE9z$" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
  </node>
</model>

