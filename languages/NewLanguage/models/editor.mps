<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:73e8819a-a717-4690-ae9d-66307c73d9ce(NewLanguage.editor)">
  <persistence version="9" />
  <languages>
    <use id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor" version="7" />
    <devkit ref="2677cb18-f558-4e33-bc38-a5139cee06dc(jetbrains.mps.devkit.language-design)" />
  </languages>
  <imports>
    <import index="8ezu" ref="r:93c6569a-4020-448e-914e-5eb8bc7a3d46(NewLanguage.structure)" implicit="true" />
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
    <import index="tpch" ref="r:00000000-0000-4000-0000-011c8959028d(jetbrains.mps.lang.structure.editor)" implicit="true" />
  </imports>
  <registry>
    <language id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor">
      <concept id="1071666914219" name="jetbrains.mps.lang.editor.structure.ConceptEditorDeclaration" flags="ig" index="24kQdi" />
      <concept id="1140524381322" name="jetbrains.mps.lang.editor.structure.CellModel_ListWithRole" flags="ng" index="2czfm3">
        <child id="1140524464360" name="cellLayout" index="2czzBx" />
      </concept>
      <concept id="1237303669825" name="jetbrains.mps.lang.editor.structure.CellLayout_Indent" flags="nn" index="l2Vlx" />
      <concept id="1237307900041" name="jetbrains.mps.lang.editor.structure.IndentLayoutIndentStyleClassItem" flags="ln" index="lj46D" />
      <concept id="1237308012275" name="jetbrains.mps.lang.editor.structure.IndentLayoutNewLineStyleClassItem" flags="ln" index="ljvvj" />
      <concept id="1237375020029" name="jetbrains.mps.lang.editor.structure.IndentLayoutNewLineChildrenStyleClassItem" flags="ln" index="pj6Ft" />
      <concept id="1237385578942" name="jetbrains.mps.lang.editor.structure.IndentLayoutOnNewLineStyleClassItem" flags="ln" index="pVoyu" />
      <concept id="1080736578640" name="jetbrains.mps.lang.editor.structure.BaseEditorComponent" flags="ig" index="2wURMF">
        <child id="1080736633877" name="cellModel" index="2wV5jI" />
      </concept>
      <concept id="1186403694788" name="jetbrains.mps.lang.editor.structure.ColorStyleClassItem" flags="ln" index="VaVBg">
        <property id="1186403713874" name="color" index="Vb096" />
      </concept>
      <concept id="1186404549998" name="jetbrains.mps.lang.editor.structure.ForegroundColorStyleClassItem" flags="ln" index="VechU" />
      <concept id="1186414536763" name="jetbrains.mps.lang.editor.structure.BooleanStyleSheetItem" flags="ln" index="VOi$J">
        <property id="1186414551515" name="flag" index="VOm3f" />
      </concept>
      <concept id="1240253180846" name="jetbrains.mps.lang.editor.structure.IndentLayoutNoWrapClassItem" flags="ln" index="34QqEe" />
      <concept id="1088013125922" name="jetbrains.mps.lang.editor.structure.CellModel_RefCell" flags="sg" stub="730538219795941030" index="1iCGBv">
        <child id="1088186146602" name="editorComponent" index="1sWHZn" />
      </concept>
      <concept id="1381004262292414836" name="jetbrains.mps.lang.editor.structure.ICellStyle" flags="ng" index="1k5N5V">
        <reference id="1381004262292426837" name="parentStyleClass" index="1k5W1q" />
      </concept>
      <concept id="1236262245656" name="jetbrains.mps.lang.editor.structure.MatchingLabelStyleClassItem" flags="ln" index="3mYdg7">
        <property id="1238091709220" name="labelName" index="1413C4" />
      </concept>
      <concept id="1088185857835" name="jetbrains.mps.lang.editor.structure.InlineEditorComponent" flags="ig" index="1sVBvm" />
      <concept id="1139848536355" name="jetbrains.mps.lang.editor.structure.CellModel_WithRole" flags="ng" index="1$h60E">
        <property id="1139852716018" name="noTargetText" index="1$x2rV" />
        <property id="1140017977771" name="readOnly" index="1Intyy" />
        <reference id="1140103550593" name="relationDeclaration" index="1NtTu8" />
      </concept>
      <concept id="1073389446423" name="jetbrains.mps.lang.editor.structure.CellModel_Collection" flags="sn" stub="3013115976261988961" index="3EZMnI">
        <child id="1106270802874" name="cellLayout" index="2iSdaV" />
        <child id="1073389446424" name="childCellModel" index="3EZMnx" />
      </concept>
      <concept id="1073389577006" name="jetbrains.mps.lang.editor.structure.CellModel_Constant" flags="sn" stub="3610246225209162225" index="3F0ifn">
        <property id="1082639509531" name="nullText" index="ilYzB" />
        <property id="1073389577007" name="text" index="3F0ifm" />
      </concept>
      <concept id="1073389658414" name="jetbrains.mps.lang.editor.structure.CellModel_Property" flags="sg" stub="730538219796134133" index="3F0A7n" />
      <concept id="1219418625346" name="jetbrains.mps.lang.editor.structure.IStyleContainer" flags="ng" index="3F0Thp">
        <child id="1219418656006" name="styleItem" index="3F10Kt" />
      </concept>
      <concept id="1073389882823" name="jetbrains.mps.lang.editor.structure.CellModel_RefNode" flags="sg" stub="730538219795960754" index="3F1sOY" />
      <concept id="1073390211982" name="jetbrains.mps.lang.editor.structure.CellModel_RefNodeList" flags="sg" stub="2794558372793454595" index="3F2HdR" />
      <concept id="1166049232041" name="jetbrains.mps.lang.editor.structure.AbstractComponent" flags="ng" index="1XWOmA">
        <reference id="1166049300910" name="conceptDeclaration" index="1XX52x" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
    </language>
  </registry>
  <node concept="24kQdi" id="RHuA0XBI53">
    <ref role="1XX52x" to="8ezu:RHuA0XBHG5" resolve="Script" />
    <node concept="3EZMnI" id="RHuA0XBI59" role="2wV5jI">
      <node concept="3F0ifn" id="RHuA0XBI5b" role="3EZMnx">
        <property role="3F0ifm" value="Script" />
      </node>
      <node concept="3F0A7n" id="RHuA0XBI5u" role="3EZMnx">
        <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
        <node concept="34QqEe" id="RHuA0XBV1c" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="RHuA0XCVaV" role="3EZMnx">
        <property role="3F0ifm" value="{" />
        <node concept="3mYdg7" id="RHuA0XCVod" role="3F10Kt">
          <property role="1413C4" value="block" />
        </node>
      </node>
      <node concept="3F2HdR" id="RHuA0XDmQh" role="3EZMnx">
        <ref role="1NtTu8" to="8ezu:RHuA0XDmPc" resolve="variable" />
        <node concept="l2Vlx" id="RHuA0XDmQk" role="2czzBx" />
        <node concept="pVoyu" id="RHuA0XDmRe" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="lj46D" id="RHuA0XDmRp" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="pj6Ft" id="RHuA0XDAYb" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F2HdR" id="RHuA0XBIkw" role="3EZMnx">
        <ref role="1NtTu8" to="8ezu:RHuA0XBHG9" resolve="loops" />
        <node concept="l2Vlx" id="RHuA0XBIky" role="2czzBx" />
        <node concept="pj6Ft" id="RHuA0XBIDZ" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="lj46D" id="RHuA0XBJoU" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="pVoyu" id="RHuA0XC6Sf" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="RHuA0XCVow" role="3EZMnx">
        <property role="3F0ifm" value="}" />
        <node concept="3mYdg7" id="RHuA0XCVoM" role="3F10Kt">
          <property role="1413C4" value="block" />
        </node>
      </node>
      <node concept="l2Vlx" id="RHuA0XBI5c" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="RHuA0XBJBj">
    <ref role="1XX52x" to="8ezu:RHuA0XBHmN" resolve="Iterate" />
    <node concept="3EZMnI" id="RHuA0XBJBl" role="2wV5jI">
      <node concept="3F0ifn" id="RHuA0XBJBs" role="3EZMnx">
        <property role="3F0ifm" value="iterate" />
        <ref role="1k5W1q" to="tpch:24YP6ZDyde4" resolve="Keyword" />
        <node concept="VechU" id="RHuA0XC2jd" role="3F10Kt">
          <property role="Vb096" value="DARK_BLUE" />
        </node>
      </node>
      <node concept="l2Vlx" id="RHuA0XBJBo" role="2iSdaV" />
      <node concept="3F1sOY" id="RHuA0XBJBR" role="3EZMnx">
        <ref role="1NtTu8" to="8ezu:RHuA0XBJBI" resolve="val" />
      </node>
      <node concept="3F1sOY" id="RHuA0XBM03" role="3EZMnx">
        <ref role="1NtTu8" to="8ezu:RHuA0XBLuR" resolve="from" />
        <ref role="1k5W1q" to="tpch:24YP6ZDyde4" resolve="Keyword" />
        <node concept="VechU" id="RHuA0XC2ji" role="3F10Kt">
          <property role="Vb096" value="DARK_GREEN" />
        </node>
      </node>
      <node concept="3F1sOY" id="RHuA0XBNJK" role="3EZMnx">
        <property role="1$x2rV" value="from" />
        <ref role="1NtTu8" to="8ezu:RHuA0XBNJs" resolve="val_from" />
      </node>
      <node concept="3F1sOY" id="RHuA0XBNJW" role="3EZMnx">
        <ref role="1NtTu8" to="8ezu:RHuA0XBNJw" resolve="to" />
        <ref role="1k5W1q" to="tpch:24YP6ZDyde4" resolve="Keyword" />
        <node concept="VechU" id="RHuA0XC3kk" role="3F10Kt">
          <property role="Vb096" value="DARK_GREEN" />
        </node>
      </node>
      <node concept="3F1sOY" id="RHuA0XBNKa" role="3EZMnx">
        <property role="1$x2rV" value="to" />
        <ref role="1NtTu8" to="8ezu:RHuA0XBNJ_" resolve="val_to" />
      </node>
      <node concept="3F0ifn" id="RHuA0XCTdv" role="3EZMnx">
        <property role="3F0ifm" value="{" />
        <node concept="3mYdg7" id="RHuA0XCTdK" role="3F10Kt">
          <property role="1413C4" value="block" />
        </node>
      </node>
      <node concept="3F2HdR" id="RHuA0XCL6Q" role="3EZMnx">
        <ref role="1NtTu8" to="8ezu:5NYde3XddE3" resolve="statement" />
        <node concept="l2Vlx" id="RHuA0XCL6U" role="2czzBx" />
        <node concept="pVoyu" id="RHuA0XCL76" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="pj6Ft" id="RHuA0XCMX2" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="lj46D" id="RHuA0XCOQj" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="RHuA0XCTe4" role="3EZMnx">
        <property role="3F0ifm" value="}" />
        <node concept="3mYdg7" id="RHuA0XCTen" role="3F10Kt">
          <property role="1413C4" value="block" />
        </node>
        <node concept="pVoyu" id="5NYde3XddFx" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="RHuA0XBKYT">
    <property role="3GE5qa" value="variables" />
    <ref role="1XX52x" to="8ezu:RHuA0XBJBD" resolve="Variable" />
    <node concept="3F0A7n" id="RHuA0XBKYV" role="2wV5jI">
      <property role="1$x2rV" value="iterated var" />
      <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
    </node>
  </node>
  <node concept="24kQdi" id="RHuA0XBOAs">
    <property role="3GE5qa" value="range iterators name" />
    <ref role="1XX52x" to="8ezu:RHuA0XBNJn" resolve="To" />
    <node concept="3F0ifn" id="RHuA0XBU5I" role="2wV5jI">
      <property role="3F0ifm" value="to" />
    </node>
  </node>
  <node concept="24kQdi" id="RHuA0XBRfm">
    <property role="3GE5qa" value="range iterators name" />
    <ref role="1XX52x" to="8ezu:RHuA0XBLuG" resolve="From" />
    <node concept="3F0ifn" id="RHuA0XBT7Y" role="2wV5jI">
      <property role="3F0ifm" value="from" />
      <property role="ilYzB" value="from" />
    </node>
  </node>
  <node concept="24kQdi" id="RHuA0XBW2w">
    <property role="3GE5qa" value="others" />
    <ref role="1XX52x" to="8ezu:RHuA0XBW2j" resolve="Body" />
    <node concept="3EZMnI" id="RHuA0XCZfb" role="2wV5jI">
      <node concept="3F2HdR" id="RHuA0XCZfi" role="3EZMnx">
        <ref role="1NtTu8" to="8ezu:RHuA0XCZeO" resolve="iterate" />
        <node concept="l2Vlx" id="RHuA0XCZfk" role="2czzBx" />
        <node concept="ljvvj" id="RHuA0XCZgw" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="pj6Ft" id="RHuA0XD3AK" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F2HdR" id="RHuA0XCZgB" role="3EZMnx">
        <ref role="1NtTu8" to="8ezu:RHuA0XCZeQ" resolve="code" />
        <node concept="l2Vlx" id="RHuA0XCZgD" role="2czzBx" />
        <node concept="pj6Ft" id="RHuA0XD3AI" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="l2Vlx" id="RHuA0XCZfe" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="RHuA0XC4yM">
    <property role="3GE5qa" value="variables" />
    <ref role="1XX52x" to="8ezu:RHuA0XC4y_" resolve="InputValue" />
    <node concept="3EZMnI" id="RHuA0XC4yU" role="2wV5jI">
      <node concept="3F0ifn" id="RHuA0XC4yW" role="3EZMnx">
        <property role="3F0ifm" value="int" />
        <node concept="VechU" id="RHuA0XC9jT" role="3F10Kt">
          <property role="Vb096" value="DARK_BLUE" />
        </node>
      </node>
      <node concept="3F0A7n" id="RHuA0XC4z8" role="3EZMnx">
        <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
      </node>
      <node concept="3F0ifn" id="RHuA0XC4zg" role="3EZMnx">
        <property role="3F0ifm" value="=" />
      </node>
      <node concept="3F0A7n" id="RHuA0XC4zq" role="3EZMnx">
        <ref role="1NtTu8" to="8ezu:RHuA0XC4yC" resolve="value" />
      </node>
      <node concept="l2Vlx" id="RHuA0XC4yX" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="RHuA0XCkIi">
    <property role="3GE5qa" value="variables" />
    <ref role="1XX52x" to="8ezu:RHuA0XCcZY" resolve="ExpressionVariable" />
    <node concept="3F1sOY" id="RHuA0XCkIk" role="2wV5jI">
      <ref role="1NtTu8" to="8ezu:RHuA0XCkI8" resolve="expression" />
    </node>
  </node>
  <node concept="24kQdi" id="RHuA0XCASP">
    <property role="3GE5qa" value="variables" />
    <ref role="1XX52x" to="8ezu:RHuA0XC$to" resolve="VariableReference" />
    <node concept="1iCGBv" id="RHuA0XCATM" role="2wV5jI">
      <ref role="1NtTu8" to="8ezu:RHuA0XC$tp" resolve="field" />
      <node concept="1sVBvm" id="RHuA0XCATO" role="1sWHZn">
        <node concept="3F0A7n" id="RHuA0XCATY" role="2wV5jI">
          <property role="1Intyy" value="true" />
          <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
          <node concept="VechU" id="RHuA0XEeB_" role="3F10Kt">
            <property role="Vb096" value="blue" />
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="RHuA0XCZf2">
    <property role="3GE5qa" value="others" />
    <ref role="1XX52x" to="8ezu:RHuA0XCZeT" resolve="SomeCode" />
    <node concept="3F0ifn" id="RHuA0XCZf4" role="2wV5jI">
      <property role="3F0ifm" value="some Java code" />
      <node concept="VechU" id="RHuA0XD5X9" role="3F10Kt">
        <property role="Vb096" value="red" />
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="RHuA0XDkKY">
    <property role="3GE5qa" value="variables" />
    <ref role="1XX52x" to="8ezu:RHuA0XDiMb" resolve="Int_Variable" />
    <node concept="3EZMnI" id="RHuA0XDkL0" role="2wV5jI">
      <node concept="3F0ifn" id="RHuA0XDkLd" role="3EZMnx">
        <property role="3F0ifm" value="int" />
        <node concept="VechU" id="RHuA0XEj_I" role="3F10Kt">
          <property role="Vb096" value="DARK_BLUE" />
        </node>
      </node>
      <node concept="3F0A7n" id="RHuA0XDkLp" role="3EZMnx">
        <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
      </node>
      <node concept="3F0ifn" id="RHuA0XDkLF" role="3EZMnx">
        <property role="3F0ifm" value="=" />
      </node>
      <node concept="3F0A7n" id="RHuA0XDkM3" role="3EZMnx">
        <ref role="1NtTu8" to="8ezu:RHuA0XDiMg" resolve="value" />
      </node>
      <node concept="l2Vlx" id="RHuA0XDkL3" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="RHuA0XDp0E">
    <property role="3GE5qa" value="variables" />
    <ref role="1XX52x" to="8ezu:RHuA0XDp0t" resolve="IntReference" />
    <node concept="1iCGBv" id="RHuA0XDp0G" role="2wV5jI">
      <ref role="1NtTu8" to="8ezu:RHuA0XDp0u" resolve="field" />
      <node concept="1sVBvm" id="RHuA0XDp0I" role="1sWHZn">
        <node concept="3F0A7n" id="RHuA0XDp0V" role="2wV5jI">
          <property role="1Intyy" value="true" />
          <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="RHuA0XDDlR">
    <property role="3GE5qa" value="variables" />
    <ref role="1XX52x" to="8ezu:RHuA0XDDlA" resolve="String_Variable" />
    <node concept="3EZMnI" id="RHuA0XDDpa" role="2wV5jI">
      <node concept="3F0ifn" id="RHuA0XDDpc" role="3EZMnx">
        <property role="3F0ifm" value="string" />
        <node concept="VechU" id="RHuA0XEm4W" role="3F10Kt">
          <property role="Vb096" value="DARK_BLUE" />
        </node>
      </node>
      <node concept="3F0A7n" id="RHuA0XDDpu" role="3EZMnx">
        <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
      </node>
      <node concept="3F0ifn" id="RHuA0XDDpK" role="3EZMnx">
        <property role="3F0ifm" value="=" />
      </node>
      <node concept="3F0A7n" id="RHuA0XDDq8" role="3EZMnx">
        <ref role="1NtTu8" to="8ezu:RHuA0XDDlB" resolve="value" />
      </node>
      <node concept="l2Vlx" id="RHuA0XDDpd" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="RHuA0XE9zK">
    <property role="3GE5qa" value="variables" />
    <ref role="1XX52x" to="8ezu:RHuA0XE9zv" resolve="Bool_Variable" />
    <node concept="3EZMnI" id="RHuA0XE9zM" role="2wV5jI">
      <node concept="3F0ifn" id="RHuA0XE9zZ" role="3EZMnx">
        <property role="3F0ifm" value="bool" />
        <node concept="VechU" id="RHuA0XEm4U" role="3F10Kt">
          <property role="Vb096" value="DARK_BLUE" />
        </node>
      </node>
      <node concept="3F0A7n" id="RHuA0XE9$b" role="3EZMnx">
        <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
      </node>
      <node concept="3F0ifn" id="RHuA0XE9$t" role="3EZMnx">
        <property role="3F0ifm" value="=" />
      </node>
      <node concept="3F0A7n" id="RHuA0XE9$P" role="3EZMnx">
        <ref role="1NtTu8" to="8ezu:RHuA0XE9zw" resolve="value" />
      </node>
      <node concept="l2Vlx" id="RHuA0XE9zP" role="2iSdaV" />
    </node>
  </node>
</model>

