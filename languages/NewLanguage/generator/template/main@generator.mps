<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:2255a8ed-e764-441e-a822-cabbe4fb4095(main@generator)">
  <persistence version="9" />
  <languages>
    <devkit ref="a2eb3a43-fcc2-4200-80dc-c60110c4862d(jetbrains.mps.devkit.templates)" />
  </languages>
  <imports>
    <import index="8ezu" ref="r:93c6569a-4020-448e-914e-5eb8bc7a3d46(NewLanguage.structure)" />
    <import index="wyt6" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.lang(JDK/)" implicit="true" />
    <import index="guwi" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.io(JDK/)" implicit="true" />
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1202948039474" name="jetbrains.mps.baseLanguage.structure.InstanceMethodCallOperation" flags="nn" index="liA8E" />
      <concept id="1239714755177" name="jetbrains.mps.baseLanguage.structure.AbstractUnaryNumberOperation" flags="nn" index="2$Kvd9">
        <child id="1239714902950" name="expression" index="2$L3a6" />
      </concept>
      <concept id="1154032098014" name="jetbrains.mps.baseLanguage.structure.AbstractLoopStatement" flags="nn" index="2LF5Ji">
        <child id="1154032183016" name="body" index="2LFqv$" />
      </concept>
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1070533707846" name="jetbrains.mps.baseLanguage.structure.StaticFieldReference" flags="nn" index="10M0yZ">
        <reference id="1144433057691" name="classifier" index="1PxDUh" />
      </concept>
      <concept id="1070534370425" name="jetbrains.mps.baseLanguage.structure.IntegerType" flags="in" index="10Oyi0" />
      <concept id="1068390468198" name="jetbrains.mps.baseLanguage.structure.ClassConcept" flags="ig" index="312cEu" />
      <concept id="1068431474542" name="jetbrains.mps.baseLanguage.structure.VariableDeclaration" flags="ng" index="33uBYm">
        <child id="1068431790190" name="initializer" index="33vP2m" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123132" name="jetbrains.mps.baseLanguage.structure.BaseMethodDeclaration" flags="ng" index="3clF44">
        <property id="4276006055363816570" name="isSynchronized" index="od$2w" />
        <property id="1181808852946" name="isFinal" index="DiZV1" />
        <child id="1068580123133" name="returnType" index="3clF45" />
        <child id="1068580123135" name="body" index="3clF47" />
      </concept>
      <concept id="1068580123165" name="jetbrains.mps.baseLanguage.structure.InstanceMethodDeclaration" flags="ig" index="3clFb_">
        <property id="1178608670077" name="isAbstract" index="1EzhhJ" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068580320020" name="jetbrains.mps.baseLanguage.structure.IntegerConstant" flags="nn" index="3cmrfG">
        <property id="1068580320021" name="value" index="3cmrfH" />
      </concept>
      <concept id="1068581242863" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclaration" flags="nr" index="3cpWsn" />
      <concept id="1068581517677" name="jetbrains.mps.baseLanguage.structure.VoidType" flags="in" index="3cqZAl" />
      <concept id="1081506773034" name="jetbrains.mps.baseLanguage.structure.LessThanExpression" flags="nn" index="3eOVzh" />
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
        <child id="1068499141038" name="actualArgument" index="37wK5m" />
      </concept>
      <concept id="1107461130800" name="jetbrains.mps.baseLanguage.structure.Classifier" flags="ng" index="3pOWGL">
        <child id="5375687026011219971" name="member" index="jymVt" unordered="true" />
      </concept>
      <concept id="7812454656619025416" name="jetbrains.mps.baseLanguage.structure.MethodDeclaration" flags="ng" index="1rXfSm">
        <property id="8355037393041754995" name="isNative" index="2aFKle" />
      </concept>
      <concept id="1081773326031" name="jetbrains.mps.baseLanguage.structure.BinaryOperation" flags="nn" index="3uHJSO">
        <child id="1081773367579" name="rightExpression" index="3uHU7w" />
        <child id="1081773367580" name="leftExpression" index="3uHU7B" />
      </concept>
      <concept id="1214918800624" name="jetbrains.mps.baseLanguage.structure.PostfixIncrementExpression" flags="nn" index="3uNrnE" />
      <concept id="1178549954367" name="jetbrains.mps.baseLanguage.structure.IVisible" flags="ng" index="1B3ioH">
        <child id="1178549979242" name="visibility" index="1B3o_S" />
      </concept>
      <concept id="1144230876926" name="jetbrains.mps.baseLanguage.structure.AbstractForStatement" flags="nn" index="1DupvO">
        <child id="1144230900587" name="variable" index="1Duv9x" />
      </concept>
      <concept id="1144231330558" name="jetbrains.mps.baseLanguage.structure.ForStatement" flags="nn" index="1Dw8fO">
        <child id="1144231399730" name="condition" index="1Dwp0S" />
        <child id="1144231408325" name="iteration" index="1Dwrff" />
      </concept>
      <concept id="1146644602865" name="jetbrains.mps.baseLanguage.structure.PublicVisibility" flags="nn" index="3Tm1VV" />
      <concept id="1146644641414" name="jetbrains.mps.baseLanguage.structure.ProtectedVisibility" flags="nn" index="3Tmbuc" />
    </language>
    <language id="b401a680-8325-4110-8fd3-84331ff25bef" name="jetbrains.mps.lang.generator">
      <concept id="1114729360583" name="jetbrains.mps.lang.generator.structure.CopySrcListMacro" flags="ln" index="2b32R4">
        <child id="1168278589236" name="sourceNodesQuery" index="2P8S$" />
      </concept>
      <concept id="1095416546421" name="jetbrains.mps.lang.generator.structure.MappingConfiguration" flags="ig" index="bUwia">
        <child id="1167328349397" name="reductionMappingRule" index="3acgRq" />
      </concept>
      <concept id="1168559333462" name="jetbrains.mps.lang.generator.structure.TemplateDeclarationReference" flags="ln" index="j$656" />
      <concept id="1168619357332" name="jetbrains.mps.lang.generator.structure.RootTemplateAnnotation" flags="lg" index="n94m4">
        <reference id="1168619429071" name="applicableConcept" index="n9lRv" />
      </concept>
      <concept id="1095672379244" name="jetbrains.mps.lang.generator.structure.TemplateFragment" flags="ng" index="raruj" />
      <concept id="1722980698497626400" name="jetbrains.mps.lang.generator.structure.ITemplateCall" flags="ng" index="v9R3L">
        <reference id="1722980698497626483" name="template" index="v9R2y" />
      </concept>
      <concept id="1167169188348" name="jetbrains.mps.lang.generator.structure.TemplateFunctionParameter_sourceNode" flags="nn" index="30H73N" />
      <concept id="1167169308231" name="jetbrains.mps.lang.generator.structure.BaseMappingRule" flags="ng" index="30H$t8">
        <reference id="1167169349424" name="applicableConcept" index="30HIoZ" />
      </concept>
      <concept id="1092059087312" name="jetbrains.mps.lang.generator.structure.TemplateDeclaration" flags="ig" index="13MO4I">
        <reference id="1168285871518" name="applicableConcept" index="3gUMe" />
        <child id="1092060348987" name="contentNode" index="13RCb5" />
      </concept>
      <concept id="1087833241328" name="jetbrains.mps.lang.generator.structure.PropertyMacro" flags="ln" index="17Uvod">
        <child id="1167756362303" name="propertyValueFunction" index="3zH0cK" />
      </concept>
      <concept id="1167327847730" name="jetbrains.mps.lang.generator.structure.Reduction_MappingRule" flags="lg" index="3aamgX">
        <child id="1169672767469" name="ruleConsequence" index="1lVwrX" />
      </concept>
      <concept id="1167756080639" name="jetbrains.mps.lang.generator.structure.PropertyMacro_GetPropertyValue" flags="in" index="3zFVjK" />
      <concept id="1167770111131" name="jetbrains.mps.lang.generator.structure.ReferenceMacro_GetReferent" flags="in" index="3$xsQk" />
      <concept id="1167951910403" name="jetbrains.mps.lang.generator.structure.SourceSubstituteMacro_SourceNodesQuery" flags="in" index="3JmXsc" />
      <concept id="1118786554307" name="jetbrains.mps.lang.generator.structure.LoopMacro" flags="ln" index="1WS0z7">
        <child id="1167952069335" name="sourceNodesQuery" index="3Jn$fo" />
      </concept>
      <concept id="1088761943574" name="jetbrains.mps.lang.generator.structure.ReferenceMacro" flags="ln" index="1ZhdrF">
        <child id="1167770376702" name="referentFunction" index="3$ytzL" />
      </concept>
    </language>
    <language id="d7706f63-9be2-479c-a3da-ae92af1e64d5" name="jetbrains.mps.lang.generator.generationContext">
      <concept id="1218047638031" name="jetbrains.mps.lang.generator.generationContext.structure.GenerationContextOp_CreateUniqueName" flags="nn" index="2piZGk">
        <child id="1218047638032" name="baseName" index="2piZGb" />
        <child id="1218049772449" name="contextNode" index="2pr8EU" />
      </concept>
      <concept id="1216860049635" name="jetbrains.mps.lang.generator.generationContext.structure.TemplateFunctionParameter_generationContext" flags="nn" index="1iwH7S" />
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1138056282393" name="jetbrains.mps.lang.smodel.structure.SLinkListAccess" flags="nn" index="3Tsc0h">
        <reference id="1138056546658" name="link" index="3TtcxE" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <child id="5169995583184591170" name="smodelAttribute" index="lGtFl" />
      </concept>
      <concept id="3364660638048049750" name="jetbrains.mps.lang.core.structure.PropertyAttribute" flags="ng" index="A9Btg">
        <property id="1757699476691236117" name="propertyName" index="2qtEX9" />
        <property id="1341860900487648621" name="propertyId" index="P4ACc" />
      </concept>
      <concept id="3364660638048049745" name="jetbrains.mps.lang.core.structure.LinkAttribute" flags="ng" index="A9Btn">
        <property id="1757699476691236116" name="linkRole" index="2qtEX8" />
        <property id="1341860900488019036" name="linkId" index="P3scX" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="bUwia" id="RHuA0XBHmd">
    <property role="TrG5h" value="main" />
    <node concept="3aamgX" id="5NYde3Xc096" role="3acgRq">
      <ref role="30HIoZ" to="8ezu:RHuA0XBHmN" resolve="Iterate" />
      <node concept="j$656" id="5NYde3Xc09c" role="1lVwrX">
        <ref role="v9R2y" node="5NYde3XbzLj" resolve="reduce_Iterate" />
      </node>
    </node>
  </node>
  <node concept="13MO4I" id="5NYde3XbzLj">
    <property role="TrG5h" value="reduce_Iterate" />
    <ref role="3gUMe" to="8ezu:RHuA0XBHmN" resolve="Iterate" />
    <node concept="1Dw8fO" id="5NYde3XbAhf" role="13RCb5">
      <node concept="raruj" id="5NYde3XbAhk" role="lGtFl" />
      <node concept="3cpWsn" id="5NYde3XbAhp" role="1Duv9x">
        <property role="TrG5h" value="i" />
        <node concept="10Oyi0" id="5NYde3XbAhz" role="1tU5fm" />
        <node concept="3cmrfG" id="5NYde3XbAhT" role="33vP2m">
          <property role="3cmrfH" value="0" />
        </node>
        <node concept="17Uvod" id="5NYde3Xc38F" role="lGtFl">
          <property role="P4ACc" value="ceab5195-25ea-4f22-9b92-103b95ca8c0c/1169194658468/1169194664001" />
          <property role="2qtEX9" value="name" />
          <node concept="3zFVjK" id="5NYde3Xc38G" role="3zH0cK">
            <node concept="3clFbS" id="5NYde3Xc38H" role="2VODD2">
              <node concept="3clFbF" id="5NYde3Xc3mv" role="3cqZAp">
                <node concept="2OqwBi" id="5NYde3Xdtan" role="3clFbG">
                  <node concept="1iwH7S" id="5NYde3XdsNf" role="2Oq$k0" />
                  <node concept="2piZGk" id="5NYde3XdtqH" role="2OqNvi">
                    <node concept="Xl_RD" id="5NYde3Xdtzp" role="2piZGb">
                      <property role="Xl_RC" value="indexVariable" />
                    </node>
                    <node concept="30H73N" id="5NYde3Xdx6A" role="2pr8EU" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3eOVzh" id="5NYde3XbB4q" role="1Dwp0S">
        <node concept="3cmrfG" id="5NYde3XbB4J" role="3uHU7w">
          <property role="3cmrfH" value="10" />
        </node>
        <node concept="37vLTw" id="5NYde3XbAi3" role="3uHU7B">
          <ref role="3cqZAo" node="5NYde3XbAhp" resolve="i" />
          <node concept="1ZhdrF" id="5NYde3XdxNx" role="lGtFl">
            <property role="P3scX" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068498886296/1068581517664" />
            <property role="2qtEX8" value="variableDeclaration" />
            <node concept="3$xsQk" id="5NYde3XdxNy" role="3$ytzL">
              <node concept="3clFbS" id="5NYde3XdxNz" role="2VODD2">
                <node concept="3clFbF" id="5NYde3XdxYG" role="3cqZAp">
                  <node concept="2OqwBi" id="5NYde3Xdyis" role="3clFbG">
                    <node concept="1iwH7S" id="5NYde3XdxYF" role="2Oq$k0" />
                    <node concept="2piZGk" id="5NYde3Xdyr6" role="2OqNvi">
                      <node concept="Xl_RD" id="5NYde3Xdysy" role="2piZGb">
                        <property role="Xl_RC" value="indexVariable" />
                      </node>
                      <node concept="30H73N" id="5NYde3Xdy$$" role="2pr8EU" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3uNrnE" id="5NYde3XbCI6" role="1Dwrff">
        <node concept="37vLTw" id="5NYde3XbCI8" role="2$L3a6">
          <ref role="3cqZAo" node="5NYde3XbAhp" resolve="i" />
          <node concept="1ZhdrF" id="5NYde3Xc6zK" role="lGtFl">
            <property role="P3scX" value="f3061a53-9226-4cc5-a443-f952ceaf5816/1068498886296/1068581517664" />
            <property role="2qtEX8" value="variableDeclaration" />
            <node concept="3$xsQk" id="5NYde3Xc6zN" role="3$ytzL">
              <node concept="3clFbS" id="5NYde3Xc6zO" role="2VODD2">
                <node concept="3clFbF" id="5NYde3XdyQF" role="3cqZAp">
                  <node concept="2OqwBi" id="5NYde3Xdz0W" role="3clFbG">
                    <node concept="1iwH7S" id="5NYde3XdyQE" role="2Oq$k0" />
                    <node concept="2piZGk" id="5NYde3Xdz9A" role="2OqNvi">
                      <node concept="Xl_RD" id="5NYde3XdzaQ" role="2piZGb">
                        <property role="Xl_RC" value="indexVariable" />
                      </node>
                      <node concept="30H73N" id="5NYde3XdzhR" role="2pr8EU" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3clFbS" id="5NYde3XbCXE" role="2LFqv$">
        <node concept="3clFbF" id="5NYde3XbCXC" role="3cqZAp">
          <node concept="2OqwBi" id="5NYde3XbDoM" role="3clFbG">
            <node concept="10M0yZ" id="5NYde3XbD2X" role="2Oq$k0">
              <ref role="3cqZAo" to="wyt6:~System.out" resolve="out" />
              <ref role="1PxDUh" to="wyt6:~System" resolve="System" />
            </node>
            <node concept="liA8E" id="5NYde3XbE2$" role="2OqNvi">
              <ref role="37wK5l" to="guwi:~PrintStream.println(java.lang.String):void" resolve="println" />
              <node concept="Xl_RD" id="5NYde3XbEzK" role="37wK5m">
                <property role="Xl_RC" value="Iterating" />
              </node>
            </node>
          </node>
          <node concept="2b32R4" id="5NYde3Xdzoj" role="lGtFl">
            <node concept="3JmXsc" id="5NYde3Xdzom" role="2P8S$">
              <node concept="3clFbS" id="5NYde3Xdzon" role="2VODD2">
                <node concept="3clFbF" id="5NYde3Xdzot" role="3cqZAp">
                  <node concept="2OqwBi" id="5NYde3Xdzoo" role="3clFbG">
                    <node concept="3Tsc0h" id="5NYde3Xd$Ck" role="2OqNvi">
                      <ref role="3TtcxE" to="8ezu:5NYde3XddE3" resolve="statement" />
                    </node>
                    <node concept="30H73N" id="5NYde3Xdzos" role="2Oq$k0" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="312cEu" id="5NYde3XdDDL">
    <property role="TrG5h" value="iterate" />
    <node concept="3clFb_" id="5NYde3XdDFe" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="perform" />
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <node concept="3clFbS" id="5NYde3XdDFh" role="3clF47">
        <node concept="3clFbF" id="5NYde3XdDFI" role="3cqZAp">
          <node concept="2OqwBi" id="5NYde3XdE41" role="3clFbG">
            <node concept="10M0yZ" id="5NYde3XdDHY" role="2Oq$k0">
              <ref role="3cqZAo" to="wyt6:~System.out" resolve="out" />
              <ref role="1PxDUh" to="wyt6:~System" resolve="System" />
            </node>
            <node concept="liA8E" id="5NYde3XdEIl" role="2OqNvi">
              <ref role="37wK5l" to="guwi:~PrintStream.println(java.lang.String):void" resolve="println" />
              <node concept="Xl_RD" id="5NYde3XdEJ3" role="37wK5m">
                <property role="Xl_RC" value="Iterating" />
              </node>
            </node>
          </node>
          <node concept="1WS0z7" id="5NYde3XdKCZ" role="lGtFl">
            <node concept="3JmXsc" id="5NYde3XdKD2" role="3Jn$fo">
              <node concept="3clFbS" id="5NYde3XdKD3" role="2VODD2">
                <node concept="3clFbF" id="5NYde3XdKD9" role="3cqZAp">
                  <node concept="2OqwBi" id="5NYde3XdKD4" role="3clFbG">
                    <node concept="3Tsc0h" id="5NYde3XdKD7" role="2OqNvi">
                      <ref role="3TtcxE" to="tpck:4uZwTti3__2" resolve="smodelAttribute" />
                    </node>
                    <node concept="30H73N" id="5NYde3XdKD8" role="2Oq$k0" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="2b32R4" id="5NYde3XdIVy" role="lGtFl">
            <node concept="3JmXsc" id="5NYde3XdIV_" role="2P8S$">
              <node concept="3clFbS" id="5NYde3XdIVA" role="2VODD2">
                <node concept="3clFbF" id="5NYde3XdIVG" role="3cqZAp">
                  <node concept="2OqwBi" id="5NYde3XdLkN" role="3clFbG">
                    <node concept="30H73N" id="5NYde3XdIVF" role="2Oq$k0" />
                    <node concept="3Tsc0h" id="5NYde3XdL_U" role="2OqNvi">
                      <ref role="3TtcxE" to="tpck:4uZwTti3__2" resolve="smodelAttribute" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tmbuc" id="5NYde3XdDEX" role="1B3o_S" />
      <node concept="3cqZAl" id="5NYde3XdDF7" role="3clF45" />
    </node>
    <node concept="3Tm1VV" id="5NYde3XdDDM" role="1B3o_S" />
    <node concept="n94m4" id="5NYde3XdDDN" role="lGtFl">
      <ref role="n9lRv" to="8ezu:RHuA0XBHmN" resolve="Iterate" />
    </node>
  </node>
</model>

